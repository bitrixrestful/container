Клонирование проекта:

-клонировать контейнер  
`git clone git@gitlab.com:bitrixrestful/container.git`

-перейти в каталог проекта  
`cd container`

-добавить подмодуль   
`git submodule update --init`

-перейти в каталог подмодуля  
`cd site`

-включить ветку routes  
`git checkout routes`

Для запуска вернуться в каталог проекта "контейнер":  
`cd ..`  
и выполнить  
`docker compose up -d`

Открыть страницу сайта в браузере (протокол http)  
http://localhost

При первом скачивании образа и запуске контейнера   получал ошибку https://screen.agimagroup.ru/ApplicationFrameHost_dX0bZ1eTun.png   
После обновления страницы ошибка не воспроизводится


Доступ к админ-панели битрикс:   
admin     
111222   
